package ru.blm.controllers;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import ru.blm.filereader.FileReader;
import ru.blm.popupwindows.LogBookWindow;
import ru.blm.textstatistic.TextStatistic;
import ru.blm.popupwindows.ErrorWindow;
import ru.blm.popupwindows.StatisticWindow;
import java.io.IOException;
import java.nio.file.Paths;

public class Controller {

    private String text;

    @FXML
    private TextArea inputText;

    @FXML
    private TextField inputPath;

    @FXML
    private RadioButton fieldRadioButton;

    @FXML
    private RadioButton fileRadioButton;

    @FXML
    void logBookButtonClick(ActionEvent event) {
        LogBookWindow.openWindow();
    }

    @FXML
    void getStatisticClick(ActionEvent event) {
        if(fieldRadioButton.isSelected()){
            text = inputText.getText();
        }
        if (fileRadioButton.isSelected()){
            try {
                text = new FileReader(Paths.get(inputPath.getText())).read();
            } catch (IOException e) {
                new ErrorWindow().openWindow("Неверные данные");
                return;
            }
        }
        StatisticWindow.openWindow(new TextStatistic(text));
    }
}
