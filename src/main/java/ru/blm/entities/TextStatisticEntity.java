package ru.blm.entities;

import lombok.Data;

import java.util.Date;

@Data
public class TextStatisticEntity {
    private Long id;
    private Date date;
    private Long countAllSymbols;
    private Long countSymbolsWithoutGaps;
    private Long countAllGaps;
    private Long countAllWords;
    private Long countAllDots;
    private Long countAllCommas;
    private Long countAllExclamationMarks;
    private Long countAllQuestionMarks;


    public TextStatisticEntity() {
    }

    public TextStatisticEntity(Long id, Date date, Long countAllSymbols, Long countSymbolsWithoutGaps, Long countAllGaps, Long countAllWords, Long countAllDots, Long countAllCommas, Long countAllExclamationMarks, Long countAllQuestionMarks) {
        this.id = id;
        this.date = date;
        this.countAllSymbols = countAllSymbols;
        this.countSymbolsWithoutGaps = countSymbolsWithoutGaps;
        this.countAllGaps = countAllGaps;
        this.countAllWords = countAllWords;
        this.countAllDots = countAllDots;
        this.countAllCommas = countAllCommas;
        this.countAllExclamationMarks = countAllExclamationMarks;
        this.countAllQuestionMarks = countAllQuestionMarks;
    }

}
