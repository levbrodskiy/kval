package ru.blm.database;


import ru.blm.entities.TextStatisticEntity;
import ru.blm.textstatistic.TextStatistic;

import java.sql.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class DataBase implements MySQLDataBaseConfig {
    private static Connection connection;

    static {
        try {
            connection = getConnection();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static Connection getConnection() throws Exception{
        Connection connection = null;
        Class.forName(DRIVER_NAME).getDeclaredConstructor().newInstance();
        connection = DriverManager.getConnection(URL, USERNAME, PASSWORD);
        return connection;
    }

    public void insertTextStatistic(TextStatistic textStatistic, Date date) throws SQLException {
        String currentDate = date.getYear() + "-" + date.getMonth() + "-" + date.getDay();
        String query = "INSERT INTO `logbook`(" +
                "`date`, " +
                "`count_all_symbols`," +
                " `count_symbols_without_gaps`, " +
                "`count_all_gaps`, " +
                "`count_all_words`, " +
                "`count_all_dots`, " +
                "`count_all_commas`," +
                " `count_all_exclamation_marks`," +
                " `count_all_question_marks`" +
                ") " +
                "VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?)";

        PreparedStatement statement = connection.prepareStatement(query);
        statement.setDate(1, new java.sql.Date(date.getTime()));
        statement.setLong(2, textStatistic.countAllSymbols());
        statement.setLong(3, textStatistic.countSymbolsWithoutGaps());
        statement.setLong(4, textStatistic.countAllGaps());
        statement.setLong(5, textStatistic.countAllWords());
        statement.setLong(6, textStatistic.countAllDots());
        statement.setLong(7, textStatistic.countAllCommas());
        statement.setLong(8, textStatistic.countAllExclamationMarks());
        statement.setLong(9, textStatistic.countAllQuestionMarks());
        statement.executeUpdate();
    }

    public List<TextStatisticEntity> getAllTextStatistic() throws SQLException {
        List<TextStatisticEntity> result = new ArrayList<>();
        String query = "select * from logbook;";
        ResultSet resultSet = connection.createStatement().executeQuery(query);

        while(resultSet.next()){
            TextStatisticEntity textStatistic = new TextStatisticEntity();
            textStatistic.setId(resultSet.getLong("id"));
            textStatistic.setDate(resultSet.getDate("date"));
            textStatistic.setCountAllSymbols(resultSet.getLong("count_all_symbols"));
            textStatistic.setCountSymbolsWithoutGaps(resultSet.getLong("count_symbols_without_gaps"));
            textStatistic.setCountAllGaps(resultSet.getLong("count_all_gaps"));
            textStatistic.setCountAllWords(resultSet.getLong("count_all_words"));
            textStatistic.setCountAllDots(resultSet.getLong("count_all_dots"));
            textStatistic.setCountAllCommas(resultSet.getLong("count_all_commas"));
            textStatistic.setCountAllExclamationMarks(resultSet.getLong("count_all_exclamation_marks"));
            textStatistic.setCountAllQuestionMarks(resultSet.getLong("count_all_question_marks"));
            result.add(textStatistic);
        }
        return result;
    }

    public void closeConnection() throws SQLException {
        connection.close();
    }
}
