package ru.blm.popupwindows;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Scene;
import javafx.scene.control.ListView;
import javafx.scene.layout.FlowPane;
import javafx.stage.Stage;
import ru.blm.database.DataBase;
import ru.blm.entities.TextStatisticEntity;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;


public class LogBookWindow {
    public static DataBase dataBase = new DataBase();

    public static void openWindow() {
        Stage stage = new Stage();
        Collection<String> logsFromDB = new ArrayList<>();
        List<String> result = parseTextStatistic();
        ObservableList<String> log = FXCollections.observableArrayList(result);
        ListView<String> logListView = new ListView<String>(log);
        logListView.setPrefWidth(1200);
        logListView.setPrefHeight(200);
        FlowPane root = new FlowPane(logListView);
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.setWidth(1225);
        stage.setTitle("Журнал");
        stage.show();
    }

    private static List<String> parseTextStatistic() {
        List<String> result = new ArrayList<>();

        try {
            List<TextStatisticEntity> list = dataBase.getAllTextStatistic();

            for (TextStatisticEntity entity : list)
            {

                String record = "Дата: " + entity.getDate() + " | " +
                    "Количество символов: " + entity.getCountAllSymbols() + " | " +
                    "Количество символов без пробелов: " + entity.getCountSymbolsWithoutGaps() + " | " +
                    "Количество пробелов: " + entity.getCountAllGaps() + " | " +
                    "Количество слов: " + entity.getCountAllWords() + " | " +
                    "Количество точек: " + entity.getCountAllDots() + " | " +
                    "Количество запятых: " + entity.getCountAllCommas() + " | " +
                    "Количество знаков восклицания: " + entity.getCountAllExclamationMarks() + " | " +
                    "Количество знаков вопроса: " + entity.getCountAllQuestionMarks();
                result.add(record);
            }
        } catch (Exception e) {
            new ErrorWindow().openWindow("Ошибка Базы Данных");
        }
        return result;
    }
}
