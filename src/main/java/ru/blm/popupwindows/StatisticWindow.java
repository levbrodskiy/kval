package ru.blm.popupwindows;

import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.stage.Stage;
import ru.blm.database.DataBase;
import ru.blm.textstatistic.TextStatistic;

import java.sql.SQLException;
import java.util.Date;
import java.util.Map;

public class StatisticWindow {
    private static DataBase dataBase = new DataBase();

    public static void openWindow(TextStatistic textStatistic) {
        Stage stage = new Stage();

        Label numberAllSymbols = new Label("Кол-во символов: " + textStatistic.countAllSymbols());
        numberAllSymbols.setLayoutX(10);
        numberAllSymbols.setLayoutY(10);

        Label numberSymbolsWithoutGaps = new Label("Кол-во символов без пробелов: " + textStatistic.countSymbolsWithoutGaps());
        numberSymbolsWithoutGaps.setLayoutX(10);
        numberSymbolsWithoutGaps.setLayoutY(40);

        Label numberAllWords = new Label("Кол-во слов: " + textStatistic.countAllWords());
        numberAllWords.setLayoutX(10);
        numberAllWords.setLayoutY(70);

        Label numberAllQuestionMarks = new Label("Кол-во вопросительных знаков: " + textStatistic.countAllQuestionMarks());
        numberAllQuestionMarks.setLayoutX(10);
        numberAllQuestionMarks.setLayoutY(100);

        Label numberAllExclamationMarks = new Label("Кол-во восклицательных знаков: " + textStatistic.countAllExclamationMarks());
        numberAllExclamationMarks.setLayoutX(10);
        numberAllExclamationMarks.setLayoutY(130);

        Label numberAllCommas = new Label("Кол-во точек: " + textStatistic.countAllDots());
        numberAllCommas.setLayoutX(10);
        numberAllCommas.setLayoutY(160);

        Label numberAllDots = new Label("Кол-во запятых: " + textStatistic.countAllCommas());
        numberAllDots.setLayoutX(10);
        numberAllDots.setLayoutY(190);

        Label numberAllGaps = new Label("Кол-во пробелов: " + textStatistic.countAllGaps());
        numberAllGaps.setLayoutX(10);
        numberAllGaps.setLayoutY(220);

        Group group = new Group(numberAllSymbols, numberSymbolsWithoutGaps, numberAllWords,
                numberAllQuestionMarks, numberAllExclamationMarks, numberAllCommas, numberAllDots, numberAllGaps);
        Scene scene = new Scene(group);

        stage.setScene(scene);
        stage.setWidth(600);
        stage.setHeight(300);

        stage.setTitle("Статистика");
        stage.show();

        try {
            dataBase.insertTextStatistic(textStatistic, new Date());
        } catch (Exception e){
            e.printStackTrace();
        }
    }
}
