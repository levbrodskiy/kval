package ru.blm.textstatistic;


import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class TextStatisticTest {
    TextStatistic textStatistic;
    TextStatistic textStatistic1;
    TextStatistic textStatistic2;
    TextStatistic textStatistic3;

    @Before
    public void setup(){
        textStatistic = new TextStatistic("/* 54к 5к ");
        textStatistic1 = new TextStatistic("85   *9 id     ");
        textStatistic2 = new TextStatistic("");
        textStatistic3 = new TextStatistic("my name is Random");
    }

    @Test
    public void countAllSymbols() {
        Assert.assertEquals(textStatistic.countAllSymbols(), 10L);
        Assert.assertEquals(textStatistic1.countAllSymbols(), 15L);
        Assert.assertEquals(textStatistic2.countAllSymbols(), 0);
        Assert.assertEquals(textStatistic3.countAllSymbols(), 17L);
    }

    @Test
    public void countSymbolsWithoutGaps() {
        Assert.assertEquals(textStatistic.countSymbolsWithoutGaps(), 7L);
        Assert.assertEquals(textStatistic1.countSymbolsWithoutGaps(), 6L);
        Assert.assertEquals(textStatistic2.countSymbolsWithoutGaps(), 0);
        Assert.assertEquals(textStatistic3.countSymbolsWithoutGaps(), 14L);
    }

    @Test
    public void countAllWords() {
        Assert.assertEquals(textStatistic.countAllWords(), 2L);
        Assert.assertEquals(textStatistic1.countAllWords(), 3L);
        Assert.assertEquals(textStatistic2.countAllWords(), 0);
        Assert.assertEquals(textStatistic3.countAllWords(), 4L);
    }

    @Test
    public void countAllGaps() {
        Assert.assertEquals(textStatistic.countAllWords(), 2L);
        Assert.assertEquals(textStatistic1.countAllWords(), 3L);
        Assert.assertEquals(textStatistic2.countAllWords(), 0);
        Assert.assertEquals(textStatistic3.countAllWords(), 4L);
    }

    @Test
    public void countAllDots() {
        Assert.assertEquals(new TextStatistic(".  .   .").countAllDots(), 3L);
        Assert.assertEquals(new TextStatistic(". sdgv gefte . , trh/.").countAllDots(), 3L);
        Assert.assertEquals(new TextStatistic("").countAllDots(), 0);
        Assert.assertEquals(new TextStatistic("..............").countAllDots(), 14L);
    }

    @Test
    public void countAllCommas() {
        Assert.assertEquals(new TextStatistic(",     r            er, ").countAllCommas(), 2L);
        Assert.assertEquals(new TextStatistic(",.,.,.,.,").countAllCommas(), 5L);
        Assert.assertEquals(new TextStatistic(" ").countAllCommas(), 0L);
        Assert.assertEquals(new TextStatistic("rrrrrrr").countAllCommas(), 0L);
    }

    @Test
    public void countAllQuestionMarks() {
        Assert.assertEquals(new TextStatistic("tyhsw?wtegergrg?egerg").countAllQuestionMarks(), 2L);
        Assert.assertEquals(new TextStatistic("?????").countAllQuestionMarks(), 5L);
        Assert.assertEquals(new TextStatistic("tyhswwtegergrg").countAllQuestionMarks(), 0L);
        Assert.assertEquals(new TextStatistic("").countAllQuestionMarks(), 0L);
    }

    @Test
    public void countAllExclamationMarks() {
        Assert.assertEquals(new TextStatistic("").countAllExclamationMarks(), 0L);
        Assert.assertEquals(new TextStatistic("fffrehetrherrgfsdgsgg").countAllExclamationMarks(), 0L);
        Assert.assertEquals(new TextStatistic("!").countAllExclamationMarks(), 1L);
        Assert.assertEquals(new TextStatistic("dtrhyrdh!ikoj!!!").countAllExclamationMarks(), 4L);
    }
}